package com.example.preexamenc1_movil;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ReciboNomina extends AppCompatActivity {

    private ReciboLogica rec;
    private TextView lblUsuario;
    private EditText lblNumRecibo, lblNombre, txtHorasNormal, txtHorasExtras, lblSubtotal, lblImpuesto, lblTotal;
    private RadioButton rdbAux, rdbAlba, rdbInge;
    private Button btnCalcular, btnLimpiar, btnRegresar;
    private RadioGroup radioGroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo_nomina);

        iniciarComponentes();

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblNombre.setText("");
                lblNumRecibo.setText("");
                txtHorasExtras.setText("");
                txtHorasNormal.setText("");
                radioGroup.clearCheck();
                lblSubtotal.setText("");
                lblImpuesto.setText("");
                lblTotal.setText("");

            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validarFaltantes()==1){
                    int puesto = capturarPuesto();
                    int horasTrabajadas = Integer.parseInt(txtHorasNormal.getText().toString());
                    int horasExtras = Integer.parseInt(txtHorasExtras.getText().toString());
                    rec.setPuesto(puesto);
                    rec.setHorasTrabNormal(horasTrabajadas);
                    rec.setHorasTrabExtras(horasExtras);


                    lblSubtotal.setText(String.valueOf(rec.calcularSubtotal()));
                    lblImpuesto.setText(String.valueOf(rec.calcularImpuesto()));
                    lblTotal.setText(String.valueOf(rec.calcularTotal()));

                }
            }
        });




        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void iniciarComponentes() {
        this.lblUsuario = findViewById(R.id.txtUsuario);
        this.lblNombre =  findViewById(R.id.txtNombre);
        this.lblNumRecibo = (EditText) findViewById(R.id.txtNumeroRecibo);
        this.txtHorasNormal = (EditText) findViewById(R.id.txtHorasTrabajadas);
        this.txtHorasExtras = (EditText) findViewById(R.id.txtHorasExtras);
        this.radioGroup = (RadioGroup) findViewById(R.id.rdbGroup);
        this.rdbAux = (RadioButton) findViewById(R.id.rdbAux);
        this.rdbAlba = (RadioButton) findViewById(R.id.rdbAlba);
        this.rdbInge = (RadioButton) findViewById(R.id.rdbIngeniero);
        this.lblSubtotal = (EditText) findViewById(R.id.txtSubtotal);
        this.lblImpuesto = (EditText) findViewById(R.id.txtImpuesto);
        this.lblTotal = (EditText) findViewById(R.id.txtTotalPago);

        this.btnCalcular = (Button) findViewById(R.id.btnCalcular);
        this.btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        this.btnRegresar = (Button) findViewById(R.id.btnRegresar);

        this.rec = new ReciboLogica();

        radioGroup.clearCheck();

        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("cliente");
        lblUsuario.setText("Usuario: " + nombre);
    }

    private int validarFaltantes(){
        for(int i=0; i<=5; i++){

            switch(i){
                case 1:
                    if (lblNombre.getText().toString().matches("")){
                        Toast.makeText(this, "Ingrese el nombre", Toast.LENGTH_SHORT).show();
                        return 0;
                    }break;

                case 2:
                    if (lblNumRecibo.getText().toString().matches("")){
                        Toast.makeText(this, "Ingrese el numero de recibo", Toast.LENGTH_SHORT).show();
                        return 0;
                    }break;
                case 3:
                    if (txtHorasNormal.getText().toString().matches("")){
                        Toast.makeText(this, "Ingrese las horas trabajadas", Toast.LENGTH_SHORT).show();
                        return 0;

                    }break;

                case 4:
                    if (txtHorasExtras.getText().toString().matches("")){
                        Toast.makeText(this, "Ingrese las horas extras", Toast.LENGTH_SHORT).show();
                        return 0;

                    }break;
                case 5:
                    if(radioGroup.getCheckedRadioButtonId()==-1){
                        Toast.makeText(this, "Seleccione un puesto", Toast.LENGTH_SHORT).show();
                        return 0;
                    }break;
                default:
                    break;

            }

        }
        return 1;
    }

    public int capturarPuesto(){
        if(rdbAux.isChecked()){
            return 0;
        } else if (rdbAlba.isChecked()){
            return 1;
        }else if (rdbInge.isChecked()){
            return 2;
        } else{
            return -1;
        }

    }


}